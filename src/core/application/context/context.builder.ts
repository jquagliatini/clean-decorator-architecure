import { Context } from "./context.interface";
import { ContextLocalStorage } from "./impl/context-local-storage";

export class ContextBuilder {
  static build(): Context {
    return new ContextLocalStorage();
  }
}
