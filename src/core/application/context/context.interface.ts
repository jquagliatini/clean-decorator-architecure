import { Session } from "../session";

type ContextUserSessionKeys = 'USER_SESSION';
type ContextUserSessionData = { email: string };

export interface Context {
  session: Session<ContextUserSessionData, ContextUserSessionKeys>;
}
