import { AsyncLocalStorage } from 'async_hooks';

import { SessionBuilder } from "../../session";
import { Context } from "../context.interface";

export class ContextLocalStorage implements Context {
  private static localStorage: AsyncLocalStorage<{ session: Context['session'] }>;

  constructor() {
    if (!ContextLocalStorage.localStorage) {
      ContextLocalStorage.localStorage = new AsyncLocalStorage();
    }

    // FIXME: For example only, should not be used here
    ContextLocalStorage.localStorage.enterWith({ session: SessionBuilder.build() });
  }

  get session(): Context['session'] {
    const { session } = ContextLocalStorage.localStorage.getStore() ?? { session: SessionBuilder.build() };
    return session;
  }
}
