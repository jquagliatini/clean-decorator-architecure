import { EventBusBuilder } from "../eventbus.builder";
import { DomainEvent } from "../eventbus.types";

type UserLoggedIn = DomainEvent<{ userId: string }, 'UserLoggedIn'>;
function userLoggedIn(userId: string): UserLoggedIn {
  return {
    type: "UserLoggedIn",
    payload: {
      userId
    }
  };
}

describe('eventbus', () => {
  it('should publish an event to all handlers', async () => {
    const spy = jest.fn();
    const bus = EventBusBuilder.build<UserLoggedIn>();

    bus.addHandler('UserLoggedIn', spy);

    const event = userLoggedIn('user-id');
    await bus.publish(event);

    expect(spy).toHaveBeenCalledWith(event);
  });
});
