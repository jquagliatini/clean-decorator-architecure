import { DomainEvent } from "./eventbus.types";
import { bus } from "./impl/eventbus-functional";

export class EventBusBuilder {
  static build<Events extends DomainEvent>() {
    return bus<Events>();
  }
}
