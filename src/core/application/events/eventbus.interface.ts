import { DomainEvent, Handler } from "./eventbus.types";

export interface EventBus<Events extends DomainEvent> {
  publish(event: DomainEvent): Promise<void>;
  addHandler(event: Events['type'], handler: Handler): void;
}
