export type DomainEvent<T = unknown, Type extends string | symbol = string> = {
  type: Type;
  payload: T;
};
export type Handler<T = unknown> = (event: DomainEvent) => T;
