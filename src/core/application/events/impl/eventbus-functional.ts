import { EventBus } from "../eventbus.interface";
import { DomainEvent, Handler } from "../eventbus.types";

export function bus<Events extends DomainEvent>(): EventBus<Events> {

  type AvailableEventTypes = Events['type'];

  const map = new Map<AvailableEventTypes, Set<Handler>>();
  return {
    async publish(event: DomainEvent): Promise<void> {
      const handlers = map.get(event.type) ?? new Set();
      await Promise.all(
        Array.from(handlers).map(handler => handler(event))
      );
    },
    addHandler(event: AvailableEventTypes, handler: Handler) {
      const handlers = map.get(event) ?? new Set<Handler>();
      handlers.add(handler);
      map.set(event, handlers);
    }
  }
}
