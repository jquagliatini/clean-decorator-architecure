import { Session } from '../../session.interface';
import { SessionMemory } from '../session-memory';

describe('SessionMemory', () => {
  let session: Session;
  beforeEach(() => {
    session = new SessionMemory();
  });

  it('should store any data', async () => {
    const session = new SessionMemory();
    await session.set('ANONYMOUS_DATA', 'any text');

    const data = await session.get('ANONYMOUS_DATA');

    expect(data).toBe('any text');
  });

  it('should store data only specified in type', async () => {
    type SessionData = {
      createdAt: Date;
      loggedAt: Date;
      isAdmin: boolean;
    };

    const session = new SessionMemory<SessionData, 'USER_SESSION'>();
    await session.set('USER_SESSION', {
      createdAt: new Date('2021-07-01T00:00:00Z'),
      loggedAt: new Date(),
      isAdmin: false,
    });
    const sessionData = await session.get('USER_SESSION');

    expect(sessionData).toMatchObject({
      loggedAt: expect.any(Date),
      createdAt: new Date('2021-07-01T00:00:00Z'),
      isAdmin: false,
    });
  });

  it('should throw if session key is undefined', async () => {
    const session = new SessionMemory();

    await expect(() => session.get('UNKNOWN_KEY')).rejects.toThrowError();
  });
});
