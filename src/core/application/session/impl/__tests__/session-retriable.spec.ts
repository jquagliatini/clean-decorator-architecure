import { mock, MockProxy } from 'jest-mock-extended';

import { Session } from '../../session.interface';
import { SessionRetriable } from '../session-retriable';

type SessionKey = 'USER_SESSION';
type SessionData = { name: string };

describe('SessionRetriable', () => {
  it('should retry the SET operation on failure', async () => {
    const baseSession: MockProxy<Session<SessionData, SessionKey>> = mock();
    baseSession.set.mockRejectedValueOnce(new Error('First failure'));
    baseSession.set.mockResolvedValue(undefined);

    const retriable = new SessionRetriable(baseSession);
    await retriable.set('USER_SESSION', { name: 'jules.verne' });

    expect(baseSession.set).toHaveBeenCalledTimes(2);
  });

  it('should retry the GET operation on failure', async () => {
    const baseSession: MockProxy<Session<SessionData, SessionKey>> = mock();
    baseSession.get.mockRejectedValueOnce(new Error('First failure'));
    baseSession.get.mockResolvedValue({ name: 'jules.verne' });

    const retriable = new SessionRetriable(baseSession);
    const data = await retriable.get('USER_SESSION');

    expect(baseSession.get).toHaveBeenCalledTimes(2);
    expect(data).toStrictEqual({ name: 'jules.verne' });
  });

  it('should retry as many times as configured, and throw when exceeded', async () => {
    const baseSession: MockProxy<Session<SessionData, SessionKey>> = mock();
    baseSession.get.mockRejectedValue(new Error('First failure'));

    const retriable = new SessionRetriable(baseSession, { attempts: 4 });

    await expect(() => retriable.get('USER_SESSION')).rejects.toThrow();
    expect(baseSession.get).toHaveBeenCalledTimes(4);
  });
});
