import { Session } from '../session.interface';

export class SessionFailingTwice<Stored = string, Keys = string>
  implements Session<Stored, Keys>
{
  private failures = {
    get: 0,
    set: 0,
  };

  constructor(private readonly origin: Session<Stored, Keys>) {}

  async get(key: Keys) {
    if (this.failures.get > 1) {
      this.failures.get = 0;
      return this.origin.get(key);
    }

    this.failures.get += 1;

    throw new Error(`Failure get ${this.failures.get}`);
  }

  async set(key: Keys, value: Stored) {
    if (this.failures.set > 1) {
      this.failures.set = 0;
      return this.origin.set(key, value);
    }

    this.failures.set += 1;

    throw new Error(`Failure set ${this.failures.set}`);
  }
}
