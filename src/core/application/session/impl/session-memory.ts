import { Session } from '../session.interface';

export class SessionMemory<Stored = string, Keys = string>
  implements Session<Stored, Keys>
{
  private readonly _map = new Map<Keys, Stored>();

  async get(key: Keys) {
    if (!this._map.has(key)) {
      throw new Error(`Could not find "${key}"`);
    }

    return this._map.get(key) as Stored;
  }

  async set(key: Keys, value: Stored) {
    this._map.set(key, value);
  }
}
