import { Session } from '../session.interface';

export class SessionRetriable<Stored = string, Keys = string>
  implements Session<Stored, Keys>
{
  private readonly attempts: number;
  private readonly verbose: boolean;

  constructor(
    private readonly origin: Session<Stored, Keys>,
    readonly options: { attempts?: number, verbose?: boolean } = {}
  ) {
    this.attempts = options.attempts ?? 3;
    this.verbose = options.verbose ?? false;
  }

  async get(key: Keys) {
    return this.retries(() => this.origin.get(key), this.attempts);
  }

  async set(key: Keys, value: Stored) {
    return this.retries(
      () => this.origin.set(key, value),
      this.attempts
    );
  }

  private retries<T>(action: () => Promise<T>, attempts: number): Promise<T> {
    this.log(`attempts ${attempts}`);
    return action().catch((e) => {
      this.warn(`Got ${e}`);

      if (attempts > 1) {
        return this.retries(action, attempts - 1);
      }

      throw e;
    });
  }

  /** TODO: extract to specific Logger class */

  private logger(level: 'WARN' | 'LOG', log: string, ...args: unknown[]) {
    if (!this.verbose) return;

    const message = ([log] as unknown[]).concat(args);
    switch (level) {
      case 'WARN':
        console.warn(...message);
      case 'LOG':
      default:
        console.log(...message);
    }
  }

  private warn(msg: string, ...args: unknown[]): void {
    this.logger('WARN', msg, ...args);
  }

  private log(msg: string, ...args: unknown[]): void {
    this.logger('LOG', msg, ...args);
  }
}
