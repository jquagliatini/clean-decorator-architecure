import { Session } from "./session.interface";
import { SessionMemory } from "./impl/session-memory";
import { SessionRetriable } from "./impl/session-retriable";

export class SessionBuilder {
  static build<SessionData = string, SessionKeys = string>(): Session<SessionData, SessionKeys> {
    return new SessionRetriable(
      new SessionMemory<SessionData, SessionKeys>()
    )
  }
}
