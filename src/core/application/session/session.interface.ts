export interface Session<Stored = string, Keys = string> {
  get(key: Keys): Promise<Stored>;
  set(key: Keys, value: Stored): Promise<void>;
}
