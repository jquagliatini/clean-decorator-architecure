import { ContextBuilder } from "./core/application/context";

async function main() {
  const ctx = ContextBuilder.build();
  await ctx.session.set('USER_SESSION', { email: 'jules.verne@email.co' });

  await wait();

  const data = await ctx.session.get('USER_SESSION');
  console.debug(data)
}

main().catch(console.error);

async function wait(ms: number = 1_000): Promise<void> {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}
